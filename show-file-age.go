package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"
	"regexp"
)

type modTimeInfo struct {
	id      int
	path    string
	modTime time.Time
	video   string
}

type htmlInfo struct {
	Path    string
	ModTime string
	Jpeg    bool
}

func main() {

	if gin.IsDebugging() {
		fmt.Print("This progamm shows only the path and the age of a file in 'STARTDIR'\n")
		fmt.Print("only the following files will be handled '.[mM][pP]4|.[mM]4[vV$|.[hH]264|.go'\n")
		fmt.Print("The git repository: https://gitlab.com/aleks001/show-files-age \n")
	}

	if len(os.Getenv("LISTEN_IP_PORT")) == 0 {
		fmt.Print("I need a ip and port on which I should listen. LISTEN_IP_PORT\n")
		os.Exit(1)
	}

	if os.Getenv("TEMPLATE_DIR") == "" {
		fmt.Print("Please define the dir where the templates are: TEMPLATE_DIR\n")
		os.Exit(1)
	}
	router := gin.Default()

	gin.DisableConsoleColor()
	router.LoadHTMLGlob(os.Getenv("TEMPLATE_DIR") + "/*")
	//router.SetHTMLTemplate(html)

	router.GET("/videoinfo", getInfo)

	router.Run(os.Getenv("LISTEN_IP_PORT"))
}

func getInfo(c *gin.Context) {

	var startdir = ""
	var hinfos []htmlInfo

	if os.Getenv("STARTDIR") != "" {
		startdir = os.Getenv("STARTDIR")
	} else if c.GetHeader("STARTDIR") != "" {
		startdir = c.GetHeader("STARTDIR")
	} else {
		c.String(http.StatusNotFound, "Startdir not found <br>\n")
		return
	}

	infos, err := walkModTime(startdir)
	if err != nil {
		fmt.Println(err)
		c.String(http.StatusInternalServerError, "A error is occured")
		return
	}

	sortModTime(infos)
	now := time.Now()
	//	fmt.Println("start at", now)
	for _, info := range infos {
		age := fmtAge(now.Sub(info.modTime))
//		fmt.Println("Age (H:M):", age, "File:", info.path)
		hinfos = append(hinfos, htmlInfo{
			Path:    info.path,
			ModTime: age},
		)
	}

	//	fmt.Print("hinfos %+v", hinfos)

	c.HTML(http.StatusOK, "index.tmpl", gin.H{
		"title":   "File info",
		"content": hinfos,
	})
}

func fmtAge(d time.Duration) string {
	d = d.Round(time.Minute)
	h := d / time.Hour
	d -= h * time.Hour
	m := d / time.Minute
	return fmt.Sprintf("%03d:%02d", h, m)
}

func sortModTime(infos []modTimeInfo) {
	sort.SliceStable(
		infos,
		func(i, j int) bool {
			return infos[i].modTime.Before(infos[j].modTime)
		},
	)
}

func isVideo(path string) bool {
	videos := []string{".mp4", ".m4v", ".h264", ".go"}
	ext := strings.ToLower(filepath.Ext(path))
	for _, video := range videos {
		if ext == video {
			return true
		}
	}
	return false
}

func walkModTime(root string) ([]modTimeInfo, error) {
	var infos []modTimeInfo

	// https://play.golang.org/p/W6WmMFIeOB
	var myhash map[string]bool

	myhash = make(map[string]bool)
	re:= regexp.MustCompile(`.*(upload|transfer)/[0-9]*$`)

	err := filepath.Walk(
		root,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.Mode().IsRegular() || info.Mode().IsDir() {
				path = filepath.Clean(path)
				if !isVideo(path) {
					if !re.MatchString(path) {
						fmt.Printf("no right dir: %s\n", path)
						return nil
					}
					fmt.Printf("no videoformat: %s\n", path)
					return nil
				}
				sep := string(filepath.Separator)
				dir := sep + `webroot` + sep + `videos` + sep
				path = strings.Replace(path, dir, sep, 2)
				splitted := strings.Split(path, sep)
//				fmt.Printf("splitted: %q\n", splitted)

				if !myhash[splitted[2]] {
					myhash[splitted[2]] = true
				} else {
					return nil
				}

				id, _ := strconv.Atoi(splitted[2])

				infos = append(infos, modTimeInfo{
					id:      id,
					path:    path,
					modTime: info.ModTime(),
					video:   path,
				},
				)
//				fmt.Printf("infos: %+v\n", infos)
			}
			return nil
		},
	)
	if err != nil {
		return nil, err
	}
	return infos, nil
}
