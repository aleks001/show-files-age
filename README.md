# show file ages

This programm just show the path the modtime and the age of a file

I use the [gin framework](https://github.com/gin-gonic/gin)

you can use the file `show-file-age.service` to run this file.

Thanks to stackoverflow user peterSO for his help.

https://stackoverflow.com/questions/47341278/how-to-format-a-duration-in-golang

# Security!
I strongly suggest to run this daemon behind a reverse proxy due 
to the fact that I haven't implemented any limits into this program.

